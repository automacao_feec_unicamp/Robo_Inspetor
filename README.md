# Objetivo do Projeto

![Video](https://gitlab.com/automacao_feec_unicamp/Robo_Inspetor/wikis/blob/video.mp4)

A presente projeto tem por objetivo o desenvolvimento/construção do projeto mecânico, hardware e software para um protótipo de robô móvel controlado pelo usuário por acesso remoto. 

Especificações do Projeto:

* Dimensões máximas: 250 x 250 x 150 mm.
* Peso máximo: 2,0 kg.
* Baixo consumo energético.
* Capaz de se locomover em ambientes desconhecidos -- instrumentado por câmera, encoder e sensor temperatura.
* Capaz de se comunicar com o usuário através da rede sem fio -- WiFi.
* Interface gráfica com o usuário de fácil compreensão e uso.

Mais informações: [wiki](https://gitlab.com/automacao_feec_unicamp/Robo_Inspetor/wikis/home)

## Autores

Lino Lopes de Barros Filho, Lucas Silva de Oliveira