from ClasseConversorAnalogicoDigital import ConversorAnalogicoDigital
from ClasseControleDirecao import ControleDirecao
from ClasseEscreveDisplay import EscreveDisplay
import time
from smbus import SMBus



class Controlador:

     cad   = None
     cdi   = None
     edi   = None
     tem   = None
     ace_x = None
     ace_y = None
     luz   = None
     dis_u = None

     def __init__(self):

         self.cad   = ConversorAnalogicoDigital()
         self.cdi   = None
         self.edi   = None
         self.tem   = -1
         self.ace_x = -1
         self.ace_y = -1
         self.luz   = -1
         self.dis_u = -1

     def get_cad(self):
         return self.cad

     def set_cad(self, cad):
         self.cad = cad

     def get_cdi(self):
         return self.cdi

     def set_cdi(self, cdi):
         self.cid = cdi

     def get_edi(self):
         return self.edi

     def set_edi(self, edi):
         self.edi = edi

     def get_tem(self):
         return self.tem

     def set_tem(self, tem):
         self.tem = tem

     def get_ace_x(self):
         return self.ace_x

     def set_ace_x(self, ace_x):
         self.ace_x = ace_x

     def get_ace_y(self):
         return self.ace_y

     def set_ace_y(self, ace_y):
         self.ace_y = ace_y
  
     def get_luz(self):
         return self.luz

     def set_luz(self, luz):
         self.luz = luz

     def get_dis_u(self):
          return self.dis_u

     def set_dis_u(self, dis_u):
          self.dis_u = dis_u

     def LerSensores(self):

         Flag = True

         while(Flag):

            try:

               nivel_digital   = self.cad.ReadChannel(0)
               self.ace_x      = self.cad.ConvertAce_X(nivel_digital,1)    
        
               nivel_digital = self.cad.ReadChannel(1)
               self.ace_y    = self.cad.ConvertAce_Y(nivel_digital,1)
        
               nivel_digital = self.cad.ReadChannel(2)
               self.luz      = self.cad.ConvertLux(nivel_digital,1)
        
               nivel_digital = self.cad.ReadChannel(3)
               self.tem      = self.cad.ConvertTemperature(nivel_digital,1)
        
               self.dis_u    =  self.cad.ReadUltra_Som(1)
               Flag = False

            except Exception:
                   Flag       = True
                   self.cad   = ConversorAnalogicoDigital()
                   print("Exception - LerSensores")

     def EscreverSensoresDisplay(self, tempo):


         self.edi = EscreveDisplay()
         Flag     = True

         while(Flag):

            try:

               self.EscreverDisplay("Temperatura", str(self.get_tem()), tempo)

               self.EscreverDisplay("Luminosidade", str(self.get_luz()), tempo)

               self.EscreverDisplay("Eixo x", str(self.get_ace_x()), tempo)

               self.EscreverDisplay("Eixo y", str(self.get_ace_y()), tempo)

               self.EscreverDisplay("Ultra s", str(self.get_dis_u()), tempo)
               Flag = False
    
            except Exception:
                    Flag       = True
                    self.edi   = EscreveDisplay()
                    self.LerSensores(2)
                    print('Exception - EscreverSensoresDisplay')
                    

     def ConstroiStr(self):
          #temperatura  
          msg1  = str(self.get_tem())

          #luminosidade
          msg2  = str(self.get_luz())

          #inclinacao x
          msg3  = str(self.get_ace_x())

          #inclinacao y  
          msg4  = str(self.get_ace_y())

          #ultra-som
          msg5  = str(self.get_dis_u()) #str(1.22)

          msg = msg1 + msg2 + msg3 + msg4 + msg5 + str(len(msg1)) + str(len(msg2)) + str(len(msg3)) + str(len(msg4)) + str(len(msg5))

          return msg
         
     def EscreverDisplay(self, string1, string2, tempo): 

         self.edi.lcd_string("" + string1 + "", self.edi.get_LCD_LINE_1())
         self.edi.lcd_string("" + string2 + "", self.edi.get_LCD_LINE_2())
         time.sleep(tempo)

     def DirecaoRobo(self, direcao):
 
         Flag        = True
         self.cdi    = ControleDirecao()
         #Flag para distanciado obstaculo (cm)
         FlagDistObt = 30

         if direcao == "frente":

            if self.get_dis_u() > FlagDistObt:
              
               while(Flag):

                 try:
                       
                     self.cdi.set_val_rd(0xC8)
                     self.cdi.set_val_re(0xC8)
                     self.cdi.frente()
                     Flag = False
                 except Exception:
                        Flag     = True
                        self.cdi = ControleDirecao()
                        print("Exception - DirecaoRobo (Frente)")   
         elif direcao == "direita":

              while(Flag):

                 try: 
                    self.cdi.set_val_rd(0x64)
                    self.cdi.set_val_re(0x64)
                    self.cdi.direita()
                    Flag = False
                 except Exception:
                        Flag     = True
                        self.cdi = ControleDirecao()
                        print("Exception - DirecaoRobo (Direita)")   
                   
         elif direcao == "esquerda":

              while(Flag):

                  try: 
                     self.cdi.set_val_rd(0x75)
                     self.cdi.set_val_re(0x75)  
                     self.cdi.esquerda()
                     Flag = False
                  except Exception:
                         Flag     = True
                         self.cdi = ControleDirecao()
                         print("Exception - DirecaoRobo (Esquerda)")      
                     
         elif direcao == "atras":

              while(Flag):

                 try: 
                    self.cdi.set_val_rd(0x32)
                    self.cdi.set_val_re(0x32)
                    self.cdi.atras()
                    Flag = False
                 except Exception:
                        Flag     = True
                        self.cdi = ControleDirecao()
                        print("Exception - DirecaoRobo (Atras)")    
                    
         elif direcao == "para":

              while(Flag):

                 try:
                      
                    self.cdi.set_val_rd(0x00)
                    self.cdi.set_val_re(0x00)
                    self.cdi.para()
                    Flag = False
                 except Exception:
                        Flag     = True
                        self.cdi = ControleDirecao()
                        print("Exception - DirecaoRobo (Parar)")

                        

         
     


             
 


         
          
             
