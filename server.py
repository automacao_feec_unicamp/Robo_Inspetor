import threading
import flask
import flask_socketio

class Server:
    def __init__(self, pipe):
        self.pipe = pipe
        # Lock used to prevent both the main server thread and the
        # communication loop to use the pipe simultaneously
        self.lock = threading.Lock()

        # Stantdard Flask/Socke.IO initialization
        self.app = flask.Flask(__name__)
        self.app.config.update(DEBUG=False, SECRET_KEY='secret!')
        self.socketio = flask_socketio.SocketIO(self.app)

        # We cannot use decorators in the class, so we add routes manually
        self.app.add_url_rule('/', 'index', self.index)
        self.socketio.on_event('connect', self.open_conection)
        self.socketio.on_event('disconnect', self.close_conection)
        self.socketio.on_event('message', self.receive_message)

        # Communication thread with the main controller
        self.thread = None

        # Start server
        self.socketio.run(self.app, host='0.0.0.0')


    # Communication loop with main controller
    def comm_loop(self):
        while True:
            msg_list = []
            # Are there any messages?
            with self.lock:
                while self.pipe.poll():
                    msg_list.append(self.pipe.recv())

            # Do something with the messages after releasing the lock
            for msg in msg_list:
                self.socketio.send(msg)

            # Make sure not to sleep for too long, or the messages will
            # accumulate in the pipe
            self.socketio.sleep(0.5)


    def index(self):
        return flask.render_template('index.html')


    def open_conection(self):
        if self.thread is None:
            self.thread = self.socketio.start_background_task(target=self.comm_loop)
        with self.lock:
            self.pipe.send('WebSocket connection opened')


    def close_conection(self):
        with self.lock:
            self.pipe.send('WebSocket connection closed')


    def receive_message(self, msg):
        with self.lock:
            self.pipe.send(msg) 


if __name__ == "__main__":
    print('The server should be started from the main contoller!')
