import multiprocessing
import time
import server
from ClasseControlador import Controlador

def init_server(pipe):
    server.Server(pipe)


if __name__ == "__main__":
    # Create a multiprocessing context
    ctx = multiprocessing.get_context('spawn')

    # Create a communication channel between processes
    parent_pipe, child_pipe = ctx.Pipe()

    # Start server in a different process
    server = ctx.Process(target=init_server, args=(child_pipe,))
    server.start()

    #Controlador
    controlador = Controlador()

    #Flag para distanciado obstaculo (cm)
    FlagDistObt = 30

    try:
        # Main control loop
        print('Starting main loop.\nTerminate with Control-C')
        last_update = time.time()
        while True:
            # Check whether the server sent us anything
            while parent_pipe.poll():
                # Get the message and do something with it
                msg = parent_pipe.recv()
                controlador.DirecaoRobo(msg)
                print('Received from server:', msg)

            # Read sensors
            controlador.LerSensores()
            now = time.time()

            # Activate actuators...
            # Caso tenho um obstáculo a uma certa distância, o robô pára
            if controlador.get_dis_u() <= FlagDistObt:
               controlador.DirecaoRobo("para") 
                

            # Send updates to server (every second is enough)
            if now - last_update >= 1:
                parent_pipe.send(controlador.ConstroiStr())
                last_update = now
            
            # We don't need to run at full speed... rest a little.
            time.sleep(0.1)

    except KeyboardInterrupt:
        # Stop server and wait for it to terminate
        print('Stopping server...')
        server.terminate()
        server.join()
        print('Server stopped')

        # Close connections, files, etc.
        parent_pipe.close()
        child_pipe.close()
        print('Gracefully terminated.')
