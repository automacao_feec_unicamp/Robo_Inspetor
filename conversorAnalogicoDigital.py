#!/usr/bin/python

import spidev
import time
import os
import math
import RPi.GPIO as GPIO

class ConversorAnalogicoDigital:

      spi  = None

      def __init__(self):

          # Open SPI bus
          self.spi = spidev.SpiDev()

          #spi.open(canal,porta cs_0 gpio 08) 
          self.spi.open(0,0)

      def get_spi(self):
          return self.spi

      def set_spi(self, spi):
          self.spi = spi

      def ReadChannel(self, channel):

          adc = self.spi.xfer2([1,(8+channel)<<4,0])
          data = ((adc[1]&3) << 8) + adc[2]
          return data

      def ConvertTemperature(self, Vo, places):

          temperature = ((Vo * 5) / float(1023))/0.010 #Convertendo digital->temperatura
          temperature = round(temperature,places)
          return temperature

      def ConvertLux(self, Vo, places):

          luz = ((Vo * 5) / float(1023)) #Convertendo digital->tensão
          return round((((2500 / luz) - 500) / 4.7),places) #Convertendo tensão->lumens

      def ConvertAce_X(self, Vo, places):

          tensao  = ((Vo * 5) / float(1023))
          angulo  = (tensao - 1.63) / 0.8
          return  round((math.asin(angulo) * 180) / 3.1415,places) 

      def ConvertAce_Y(self, Vo, places):
           
          tensao  = ((Vo * 5) / float(1023))
          angulo  = (tensao - 1.73) / 0.8
          return  round((math.asin(angulo) * 180) / 3.1415,places)
             
      def ReadUltra_Som(self, places):

          # Use BCM GPIO references
          # instead of physical pin numbers
          GPIO.setmode(GPIO.BCM)

          # Define GPIO to use on Pi
          GPIO_TRIGGER = 23
          GPIO_ECHO = 24

          # Set pins as output and input
          GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  # Trigger
          GPIO.setup(GPIO_ECHO,GPIO.IN)      # Echo

          # Set trigger to False (Low)
          GPIO.output(GPIO_TRIGGER, False)

          # Allow module to settle
          time.sleep(0.5)

          # Send 10us pulse to trigger
          GPIO.output(GPIO_TRIGGER, True)
          time.sleep(0.00001)
          GPIO.output(GPIO_TRIGGER, False)
          start = time.time()

          while GPIO.input(GPIO_ECHO)==0:
                start = time.time()

          while GPIO.input(GPIO_ECHO)==1:
                stop = time.time()

          # Calculate pulse length
          elapsed = stop-start

          # Distance pulse travelled in that time is time
          # multiplied by the speed of sound (cm/s)
          distance = elapsed * 34000

          # That was the distance there and back so halve the value
          distance = distance / 2

          # Reset GPIO settings
          GPIO.cleanup()

          return round(distance, places)
