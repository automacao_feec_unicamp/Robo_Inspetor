from smbus import SMBus
from time import sleep
from struct import *
import sys

class LeituraEnconder:

      bus       = None 

      def __init__(self):

          self.bus = SMBus(1)# Port 1 used on REV2

      def get_bus(self):
          return  self.bus

      def set_bus(self, bus):
          self.bus = bus


      def lerEnconder(self, endereco):

          pulsos   = -1
          qtd_er   = 0
          Flag     = True

          while(Flag):  

              data = []
              
              try:
 
                 for _ in range(4):
                     data.append(self.bus.read_byte(endereco))
                     sleep(0.001)
                 pulsos = unpack('<i', bytearray(data))
                 pulsos = pulsos[0]
                 Flag   = False
              except:
                 self.bus.write_byte(endereco, 0xFA)
                 if qtd_er < 10:
                    Flag = True
                    qtd_er += 1
                 else:   
                    pulsos = -1
                    Flag = False
             
          return pulsos 
