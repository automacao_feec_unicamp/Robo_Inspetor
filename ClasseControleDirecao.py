from smbus import SMBus
from time import sleep

class ControleDirecao:

      bus      = None 
      addr_rd  = None #Endereco da roda direita
      val_rd   = None #Dado enviado para a roda direita
      addr_re  = None #Endereco da roda esquerda
      val_re   = None #Dado enviado para a roda esquerda

      def __init__(self):

          self.bus      = SMBus(1) # Port 1 used on REV2
          self.addr_rd  = 0x58
          self.val_rd   = None
          self.addr_re  = 0x50
          self.val_re   = None

      def get_bus(self):
          return  self.bus

      def set_bus(self, bus):
          self.bus = bus

      def get_addr_rd(self):
          return self.addr_rd

      def set_addr_rd(self, addr_rd):
          self.addr_rd = addr_rd

      def get_val_rd(self):
          return self.val_rd

      def set_val_rd(self, val_rd):
          self.val_rd = val_rd

      def get_addr_re(self):
          return self.addr_re

      def set_addr_re(self, addr_re):
          self.addr_re = addr_re

      def get_val_re(self):
          return self.val_re

      def set_val_re(self, val_re):
          self.val_re = val_re  

      def frente(self):

          self.bus.write_byte(self.get_addr_rd(), self.get_val_rd())
          self.bus.write_byte(self.get_addr_re(), self.get_val_re())  

      def direita(self):

          self.bus.write_byte(self.get_addr_rd(), self.get_val_rd())
          self.bus.write_byte(self.get_addr_re(), self.get_val_re())  

      def esquerda(self):

          self.bus.write_byte(self.get_addr_rd(), self.get_val_rd())
          self.bus.write_byte(self.get_addr_re(), self.get_val_re())  
            
      def atras(self):  

          self.bus.write_byte(self.get_addr_rd(), self.get_val_rd())
          self.bus.write_byte(self.get_addr_re(), self.get_val_re())  

      def para(self):

          self.bus.write_byte(self.get_addr_rd(), self.get_val_rd())
          self.bus.write_byte(self.get_addr_re(), self.get_val_re())
     
