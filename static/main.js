       
      var socket = new io("http://" + document.domain + ":" + location.port);
      socket.on('connect', function(){console.log("connected");}); 
      socket.on('message', function(msg){
           
           var lenstr = msg.length;
           var t5     = parseInt(msg.substr(lenstr-1));  
           var t4     = parseInt(msg.substr(lenstr-2,1));  
           var t3     = parseInt(msg.substr(lenstr-3,1));  
           var t2     = parseInt(msg.substr(lenstr-4,1));  
           var t1     = parseInt(msg.substr(lenstr-5,1));  

           document.getElementById("temperatura").innerHTML  = msg.substr(0,t1);
           document.getElementById("luminosidade").innerHTML = msg.substr(t1,t2);
	   document.getElementById("inclinacaox").innerHTML  = msg.substr(t1+t2,t3);
           document.getElementById("inclinacaoy").innerHTML  = msg.substr(t1+t2+t3,t4);
           document.getElementById("ultrasom").innerHTML     = msg.substr(t1+t2+t3+t4,t5);

      });
      
      function frente(){
          socket.emit("message", "frente");
      }

      function esquerda(){
          socket.emit("message", "esquerda");
      }

      function direita(){
          socket.emit("message", "direita");
      }

      function atras(){
          socket.emit("message", "atras");
      }

      function para(){
          socket.emit("message", "para");
      }
